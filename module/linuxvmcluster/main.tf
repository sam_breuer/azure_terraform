resource "azurerm_linux_virtual_machine_scale_set" "azvmset" {
  name = "${var.prefix}-vm"
  location = var.location
  resource_group_name = azurerm_resource_group.main.name
  instances = var.nodes
  sku = "Standard_D2s_v3"
  admin_username = "testadmin"
  admin_password = "Password1234!"
  # automatic rolling upgrade
  upgrade_mode = "Rolling"
  disable_password_authentication = false
  source_image_id = var.imageid
  # required when using rolling upgrade policy
  health_probe_id = azurerm_lb_probe.eplbprobe.id
  network_interface {
    name = "${var.prefix}azvmnic"
    primary = "true"
    ip_configuration {
      name = "internal"
      primary = true
      subnet_id = var.subnetid
      load_balancer_backend_address_pool_ids = [
        azurerm_lb_backend_address_pool.bpepool.id]
      load_balancer_inbound_nat_rules_ids = [
        azurerm_lb_nat_pool.lbnatpool.id]
    }
  }
  os_disk {
    caching           = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }
  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.diagstorageacct.primary_blob_endpoint
  }
  rolling_upgrade_policy {
    max_batch_instance_percent = 50
    max_unhealthy_instance_percent = 50
    max_unhealthy_upgraded_instance_percent = 50
    pause_time_between_batches = "PT10M"
  }
  depends_on = [azurerm_lb_rule.main]
  tags = {
    Name = var.prefix
  }
}