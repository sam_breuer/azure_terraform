variable "prefix" {
  default = "eptest"
}
variable "location" {
  default = "eastus2"
}
variable "subnetid" {
  default = ""
}