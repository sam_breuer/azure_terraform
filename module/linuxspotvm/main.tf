resource "azurerm_linux_virtual_machine" "azvm" {
    name                  = "${var.prefix}-vm"
    location              = var.location
    resource_group_name   = azurerm_resource_group.main.name
    network_interface_ids = [azurerm_network_interface.nic.id]
    size                  = var.size
    admin_username = "epromoadmin"
    admin_password = "Br@nd3dPr0duct$"
    computer_name = "${var.prefix}-vm"
    priority = "Spot"
    eviction_policy = "Deallocate"
    source_image_id = var.imageid
    disable_password_authentication = false
    os_disk {
        name = "${var.prefix}-vmdisk"
        caching = "ReadWrite"
        disk_size_gb = "64"
        storage_account_type = "Premium_LRS"
    }

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.diagstorageacct.primary_blob_endpoint
    }

    tags = {
        environment = var.prefix
    }
}