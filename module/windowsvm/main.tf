resource "azurerm_windows_virtual_machine" "azvm" {
    name                  = "motqa-pc"
    location              = var.location
    resource_group_name   = azurerm_resource_group.main.name
    network_interface_ids = [azurerm_network_interface.nic.id]
    size                  = var.size
    admin_username = "epromoadmin"
    admin_password = "Br@nd3dPr0duct$"
    enable_automatic_updates = "true"
    os_disk {
          caching = "ReadWrite"
          storage_account_type = "Premium_LRS"
      }
    source_image_reference {
      publisher = "MicrosoftWindowsServer"
      offer     = "WindowsServer"
      sku       = "2019-Datacenter"
      version   = "latest"
    }
    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.diagstorageacct.primary_blob_endpoint
    }
    tags = {
        environment = var.prefix
    }
}