resource "azurerm_windows_virtual_machine" "azvm" {
    name                  = "motqasql-pc"
    location              = var.location
    resource_group_name   = azurerm_resource_group.main.name
    network_interface_ids = [azurerm_network_interface.nic.id]
    size                  = var.size
    admin_username = "epromoadmin"
    admin_password = "Br@nd3dPr0duct$"
    enable_automatic_updates = "true"
    os_disk {
          caching = "ReadWrite"
          storage_account_type = "Premium_LRS"
          disk_size_gb = "480"
      }
    source_image_reference {
      publisher = "MicrosoftSQLServer"
      offer     = "sql2019-ws2019"
      sku       = "sqldev"
      version   = "15.0.200512"
    }

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.diagstorageacct.primary_blob_endpoint
    }
    tags = {
        environment = var.prefix
    }
}
resource "azurerm_mssql_virtual_machine" "motsql" {
  virtual_machine_id               = azurerm_windows_virtual_machine.azvm.id
  sql_license_type = "PAYG"
  sql_connectivity_update_password = "M0t1v4t0r$Pr0duct5"
  sql_connectivity_update_username = "motadmin"
}