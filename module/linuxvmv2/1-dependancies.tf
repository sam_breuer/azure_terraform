
resource "azurerm_resource_group" "main" {
  name     = var.prefix
  location = var.location
}
##network access/security groups
resource "azurerm_network_security_group" "nsg" {
    name                = "${var.prefix}-NetworkSecurityGroup"
    location            = var.location
    resource_group_name = azurerm_resource_group.main.name
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "209.237.103.162"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "HTTP"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "209.237.103.162"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "HTTPS"
        priority                   = 1003
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "209.237.103.162"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "RDP"
        priority                   = 1004
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "3389"
        source_address_prefix      = "209.237.103.162"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "Alfresco"
        priority                   = 1005
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "8081"
        source_address_prefix      = "209.237.103.162"
        destination_address_prefix = "*"
    }
}
#public ip
resource "azurerm_public_ip" "publicip" {
  name = "${var.prefix}-publicip"
  location = var.location
  resource_group_name = azurerm_resource_group.main.name
  allocation_method = "Dynamic"
  domain_name_label = var.prefix
  tags = {
    Name = var.prefix
  }
}
#nic
resource "azurerm_network_interface" "nic" {
  name                = "${var.prefix}-nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.main.name

  ip_configuration {
    name                          = "nicconfig1"
    subnet_id                     = var.subnetid
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.publicip.id
  }
  tags = {
    Name = var.prefix
  }
}
resource "azurerm_network_interface_security_group_association" "nicnsg" {
  network_interface_id      = azurerm_network_interface.nic.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}
##environment diagnostic storage account
resource "azurerm_storage_account" "diagstorageacct" {
  name = "${var.prefix}diag"
  resource_group_name = azurerm_resource_group.main.name
  location = var.location
  account_replication_type = "LRS"
  account_tier = "Standard"


  tags = {
    environment = var.prefix
  }
}