$ARM_SUBSCRIPTION_ID=az keyvault secret show --name ARM-SUBSCRIPTION-ID --vault-name tstate-terraform --query value -o tsv
$ARM_CLIENT_ID=az keyvault secret show --name ARM-CLIENT-ID --vault-name tstate-terraform --query value -o tsv
$ARM_CLIENT_SECRET=az keyvault secret show --name ARM-CLIENT-SECRET --vault-name tstate-terraform --query value -o tsv
$ARM_TENANT_ID=az keyvault secret show --name ARM-TENANT-ID --vault-name tstate-terraform --query value -o tsv