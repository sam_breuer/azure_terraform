##Epromos QA environment
module tcatqa {
    source = "./module/linuxvmv2"
    prefix = "${var.prefix}eptomcat"
    resource_group = azurerm_resource_group.main.name
    subnetid = azurerm_subnet.epsubnet.id
    imageid = azurerm_image.tomcat.id
    size = "Standard_DS2_v2"
}
module admqa {
    source = "./module/linuxvmv2"
    prefix = "${var.prefix}epadm"
    resource_group = azurerm_resource_group.main.name
    subnetid = azurerm_subnet.epsubnet.id
    imageid = azurerm_image.tomcat.id
    size = "Standard_DS2_v2"
}
module endeca {
    source = "./module/linuxvmv2"
    prefix = "${var.prefix}ependeca"
    resource_group = azurerm_resource_group.main.name
    subnetid = azurerm_subnet.epsubnet.id
    imageid = azurerm_image.epromosqaendeca.id
    size = "Standard_DS2_v2"
}
module database {
    source = "./module/database"
    prefix = "${var.prefix}epmysql"
    resource_group = azurerm_resource_group.main.name
    subnetid = azurerm_subnet.epsubnet.id
}

module epwordpress {
    source = "./module/linuxvmv2"
    prefix = "${var.prefix}epwp"
    resource_group = azurerm_resource_group.main.name
    subnetid = azurerm_subnet.epsubnet.id
    imageid = azurerm_image.wordpress.id
    size = "Standard_DS2_v2"
}

##MOT QA Environment
module motqa {
    source = "./module/windowsvm"
    prefix = "${var.prefix}motqa"
    subnetid = azurerm_subnet.motsubnet.id
    size = "Standard_DS3_v2"
}
module motdatabase {
    source = "./module/windowsvmsql"
    prefix = "${var.prefix}motsql"
    subnetid = azurerm_subnet.motsubnet.id
    size = "Standard_DS3_v2"
}
module motqaes {
    source = "./module/windowsvmes"
    prefix = "${var.prefix}motqaes"
    subnetid = azurerm_subnet.motsubnet.id
    size = "Standard_DS3_v2"
}

##Inventory MGMT Machine
module snipeit {
    source = "./module/linuxvmv2"
    prefix = "${var.prefix}inventory"
    resource_group = azurerm_resource_group.main.name
    subnetid = azurerm_subnet.epsubnet.id
    imageid = azurerm_image.centos8.id
    size = "Standard_DS1_v2"
}

##Web crawler and sitemap generator
module screamingfrog {
    source = "./module/linuxvmv2"
    prefix = "${var.prefix}sfrog"
    resource_group = azurerm_resource_group.main.name
    subnetid = azurerm_subnet.subnet.id
    imageid = azurerm_image.screamingfrog.id
    size = "Standard_DS3_v2"
}